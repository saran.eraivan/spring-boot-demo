package com.neevclosure.springbootassignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {

    @Autowired
    TodoRepository todoRepository;

    public List<Todo> getAllTodos() {
        return todoRepository.findAll();
    }

    public Todo addTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    public Todo updateTodo(String identifier, Todo todo) {
        Todo todoById = todoRepository.findById(identifier).get();
        todoById.setDescription(todo.getDescription());
        todoRepository.save(todoById);
        return todoById;
    }

    public void deleteTodo(String any) {
        todoRepository.deleteById(any);
    }
}
