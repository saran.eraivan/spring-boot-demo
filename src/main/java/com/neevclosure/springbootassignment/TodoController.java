package com.neevclosure.springbootassignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoController {

    @Autowired
    TodoService todoService;

    @GetMapping("/todo")
    public List<Todo> getTodo() {
        return todoService.getAllTodos();
    }

    @PostMapping("/todo")
    public Todo addTodo(@RequestBody Todo todo) {
        return todoService.addTodo(todo);
    }

    @PutMapping("/todo/{name}")
    public Todo updateTodoByName(@PathVariable String name, @RequestBody Todo todo) {
        return todoService.updateTodo(name, todo);
    }

    @DeleteMapping("/todo/{name}")
    public ResponseEntity<?> deleteTodoByName(@PathVariable String name) {
        todoService.deleteTodo(name);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }


}
