CREATE TABLE todo (
    name VARCHAR(100) NOT NULL PRIMARY KEY,
    description text,
    is_done BOOL DEFAULT FALSE
);