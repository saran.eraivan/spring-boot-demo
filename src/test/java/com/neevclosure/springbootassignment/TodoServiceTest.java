package com.neevclosure.springbootassignment;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TodoServiceTest {

    @Mock
    TodoRepository todoRepository;

    @InjectMocks
    TodoService todoService;

    @Test
    void getAllTodosFromRepository() {
        List<Todo> expectedTodo = List.of(
                new Todo("todo1"),
                new Todo("todo2")
        );

        when(todoRepository.findAll()).thenReturn(expectedTodo);

        List<Todo> allTodos = todoService.getAllTodos();
        assertEquals(expectedTodo, allTodos);

        verify(todoRepository, times(1)).findAll();
    }

    @Test
    void addTodoIntoRepository() {
        Todo todoToBeSaved = new Todo("Todo-1");

        when(todoRepository.save(any(Todo.class))).thenReturn(todoToBeSaved);

        Todo todoReturnedAfterSaving = todoService.addTodo(todoToBeSaved);
        assertEquals(todoToBeSaved, todoReturnedAfterSaving);

        verify(todoRepository, times(1)).save(todoToBeSaved);
    }

    @Test
    void updateTodoIntoRepositoryUsingNameId() {
        Todo todoBeforeUpdation = new Todo("Todo-1");
        Todo todoToBeUpdatedWith = new Todo("Todo-1", "Todo-1 description");
        todoService.addTodo(todoBeforeUpdation);

        when(todoRepository.findById(any(String.class))).thenReturn(Optional.of(todoToBeUpdatedWith));

        Todo todoReturnedAfterModification = todoService.updateTodo(todoBeforeUpdation.getName(), todoToBeUpdatedWith);
        assertEquals(todoToBeUpdatedWith, todoReturnedAfterModification);

        verify(todoRepository, times(1)).findById(todoBeforeUpdation.getName());
    }

    @Test
    void deleteTodoFromRepositoryUsingNameId() {
        Todo todoToBeDeleted = new Todo("todo-1");

        todoService.deleteTodo(todoToBeDeleted.getName());

        verify(todoRepository, times(1)).deleteById(todoToBeDeleted.getName());
    }
}