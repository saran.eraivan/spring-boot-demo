package com.neevclosure.springbootassignment;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
class TodoControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    TodoService todoService;

    @Test
    void getAllTodosUponGetRequest() throws Exception {
        when(todoService.getAllTodos()).thenReturn(List.of(
                new Todo("todo1"),
                new Todo("todo2")
        ));

        mockMvc.perform(MockMvcRequestBuilders.get("/todo"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(2));
    }

    @Test
    public void addTodoUponPostRequest() throws Exception {
        Todo todo = new Todo("todo1");

        when(todoService.addTodo(any(Todo.class))).thenReturn(todo);

        mockMvc.perform(MockMvcRequestBuilders.post("/todo")
                        .content(asJsonString(todo))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.description").value(todo.getDescription()));
    }

    @Test
    void updateTodoUponPostRequestWithIdentifier() throws Exception {
        Todo todoBeforeUpdation = new Todo("todo1");
        todoService.addTodo(todoBeforeUpdation);
        Todo todoToBeUpdatedWith = new Todo("todo1", "todo1 description");

        when(todoService.updateTodo(any(String.class), any(Todo.class))).thenReturn(todoToBeUpdatedWith);

        mockMvc.perform(MockMvcRequestBuilders.put("/todo/" + todoBeforeUpdation.getName())
                        .content(asJsonString(todoToBeUpdatedWith))
                        .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.description").value(todoToBeUpdatedWith.getDescription()));
    }

    @Test
    void deleteATodoWhenNameIdIsGivenUponDeleteRequest() throws Exception {
        Todo todoToBeDeleted = new Todo("todo-1");
        mockMvc.perform(MockMvcRequestBuilders.delete("/todo/" + todoToBeDeleted.getName()))
                .andExpect(status().isNoContent());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
