package com.neevclosure.springbootassignment;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TodoRepositoryTest {

    @Autowired
    TodoRepository todoRepository;

    @Test
    public void testFindAll() {
        Todo todo = new Todo("Todo-1");

        todoRepository.save(todo);
        List<Todo> all = todoRepository.findAll();

        assertEquals(all, List.of(todo));
    }

    @Test
    public void testAddTodo() {
        Todo todoToBeSaved = new Todo("Todo-1");

        todoRepository.save(todoToBeSaved);
        List<Todo> todoList = todoRepository.findAll();

        assertEquals(todoList, List.of(todoToBeSaved));
        assertEquals(1, todoList.size());
    }

    @Test
    void shouldNotAddATodoTwice() {
        Todo todoToBeSaved = new Todo("Todo-1");

        todoRepository.save(todoToBeSaved);
        todoRepository.save(todoToBeSaved);
        List<Todo> todoList = todoRepository.findAll();

        assertEquals(todoList, List.of(todoToBeSaved));
        assertEquals(1, todoList.size());
    }

    @Test
    void shouldUpdateATodoById() {
        Todo todoToBeAlreadyPresent = new Todo("Todo-1");
        Todo todoToBeUpdated = new Todo("Todo-1", "Todo-1 description");
        todoRepository.save(todoToBeAlreadyPresent);

        todoRepository.save(todoToBeUpdated);
        List<Todo> todoList = todoRepository.findAll();

        assertEquals(todoList, List.of(todoToBeUpdated));
        assertEquals(todoToBeUpdated.getDescription(), todoRepository.getById(todoToBeAlreadyPresent.getName()).getDescription());
    }

    @Test
    void shouldDeleteATodoById() {
        Todo todoToBeDeleted = new Todo("Todo-1");
        Todo anotherTodo = new Todo("Todo-2");
        todoRepository.save(todoToBeDeleted);
        todoRepository.save(anotherTodo);

        todoRepository.deleteById(todoToBeDeleted.getName());

        assertEquals(todoRepository.findAll(), List.of(anotherTodo));
    }
}